#!/bin/bash

dpkg -s "mdk3" &> /dev/null

if [ $? -eq 0 ]; then
    echo "mdk3  is installed!"
else
    echo "installing mdk3 ..."
    sudo apt install mdk3 -y
fi

