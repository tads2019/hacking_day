#!/bin/bash

sudo airmon-ng start $(echo /sys/class/net/*/wireless | awk -F'/' '{ print $5 }')
sudo airmon-ng check-kill
