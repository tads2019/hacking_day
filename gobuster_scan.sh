#!/bin/bash

# contribuitors: Felipe Melchior (hdrn), Rafael Fernandes, kd

# Scan de diretorios simples 
# ./gobuster_scan.sh http://host.com third-party/directory-list.txt

# Scan de arquivos com a extensao desejada (adiciona a extens�o a cada linha da wordlist)
# ./gobuster_scan.sh http://host.com directory-list.txt .php

[ $1 ] && [ $2 ] || { echo -e "Usage: $0 <host> <wordlist (e.g. third_party/wordlist.txt)> <optional:extension>"; exit; }

GOB=`which gobuster`
if [ "$GOB" == "" ]
then
    GO=$(which go)
    if [ "$GO" == "" ] 
    then
        echo ""
        echo "First, we need to install Go (golang) on your system"
        echo ""
        sleep 2
        sudo apt-get install golang-google-api-dev
    fi
    echo ""
    echo "Now, we will install gobuster"
    echo ""
    sleep 2
    git clone https://github.com/OJ/gobuster
    pushd gobuster
    export GOPATH=`which go | sed 's/\(^.*\)\/bin\/.*$/\1/'`
    export GOROOT=$GOPATH
    export PATH=$PATH:$GOPATH/bin
    BASHRC_CHECK=$(grep -w GOPATH ~/.bashrc)
    if [ "$BASHRC_CHECK" == "" ]
    then
        echo "export GOPATH=$GOPATH" >> ~/.bashrc
        echo "export PATH=$PATH:$GOPATH/bin" >> ~/.bashrc
    fi
    go get && go build
    [ $? -eq 0 ] || { echo "[ERROR] building coud NOT finish (error: $?)"; exit; }
    popd 
else
	if [ $3 ]
	then
	    gobuster -u $1 -r -w $2 -X $3
	else
	    gobuster -u $1 -r -w $2
	fi
fi
