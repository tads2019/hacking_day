#!/bin/bash

# contributors: kd, 

[ $1 ] && [ $2 ] || { echo "Usage: $0 <server.domain> <server.port>"; exit; }

SERVER=$1
PORT=$2

TMPFILE=`mktemp`
LOGDIR=logs
LOGFILE=$LOGDIR/$SERVER.log

[ -d $LOGDIR ] || { mkdir $LOGDIR; }

[ ! -f $LOGFILE ] || { rm -f $LOGFILE; }

CIPHERS=$(openssl ciphers 'ALL:eNULL' | sed -e 's/:/ /g' | sort)

echo "======================================================="
echo -n "$SERVER : "
date +%S
echo "======================================================="

echo "-------------------------------------------------------" >> $LOGFILE
echo -n "$SERVER : "
date >> $LOGFILE
echo "-------------------------------------------------------" >> $LOGFILE
for CIPHER in ${CIPHERS[@]}
do
    echo "-------------------------------------------------------" >> $LOGFILE
    echo -n "SERVER = $SERVER : CIPHER = $CIPHER : STATUS = "
    echo -n | timeout 10 openssl s_client -cipher "$CIPHER" -connect $SERVER:$PORT &> $TMPFILE
    if [ $? -eq 0 ] 
    then
        echo "OK"
    else
        echo "NACK"
    fi
    cat $TMPFILE >> $LOGFILE
    echo "-------------------------------------------------------" >> $LOGFILE
done
echo "-------------------------------------------------------" >> $LOGFILE
echo -n "$SERVER : "
date >> $LOGFILE
echo "-------------------------------------------------------" >> $LOGFILE

echo "======================================================="
echo -n "$SERVER : "
date +%S
echo "log file: $LOGFILE"
echo "======================================================="


