#!/bin/bash

# contributors: kd, 

[ $1 ] || { echo "Usage: $0 <network (e.g. \"192.168.1.0/24\", \"192.168.1.1/32\">"; exit; }

MAC_FILE=.list_of_MACs

netdiscover -P -r "$1" | grep -E "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | awk '{print $2}' | sed 's/\(..\):\(..\):\(..\):\(..\):\(..\):\(..\).*$/\1\2\3,\1:\2:\3:\4:\5:\6/g' > $MAC_FILE

if [ ! -f oui.txt ]
then
	wget --output-file=/dev/null --output-document=oui.txt http://standards-oui.ieee.org/oui.txt
fi

while read LINE
do
	MAC=$(echo $LINE | cut -d"," -f2 | tr '[:lower:]' '[:upper:]') 
	PART=$(echo $LINE | cut -d"," -f1)
	MF=$(grep -w $PART oui.txt)
	if [ "$MF" == "" ]
	then
		MF="Unknown"
	fi 
	echo $MAC $MF
done < $MAC_FILE

rm -f $MAC_FILE
