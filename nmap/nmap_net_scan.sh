#!/bin/bash

# contributors: kd, 

[ $1 ] || { echo "Usage: $0 <network (e.g. \"192.168.1.0/24\")>"; exit; }

[ -d logs ] || { mkdir logs; }
TS=`date +%Y%m%d%H%M%S`
LOG_FILE="logs/nmap-$TS.log"

nmap -oN $LOG_FILE -p "$1"

echo ""
echo "log file: $LOG_FILE"
echo ""

