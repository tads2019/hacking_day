#!/bin/bash

# contributors: kd, 

[ $1 ] || { echo "Usage: $0 <host|network|file.txt>"; exit; }

[ -d logs ] || { mkdir logs; }
TS=`date +%Y%m%d%H%M%S`
LOG_FILE="logs/nmap-$TS.log"

NMAP_CMD="nmap -oN $LOG_FILE -A -T4"

if [[ "$1" =~ ".*/.*" ]]
then
	$NMAP_CMD -p "$1"
elif [ -f "$1" ]
then
	$NMAP_CMD -iL "$1"
else
	$NMAP_CMD $1
fi

echo ""
echo "log file: $LOG_FILE"
echo ""

