#!/bin/bash

# contributors: kd, 

[ $1 ] && [ $2 ] || { echo "Usage: $0 <host|network|file.txt> <results_format: txt|xml>"; exit; }

[ -d logs ] || { mkdir logs; }
TS=`date +%Y%m%d%H%M%S`
LOG_FILE="logs/nmap-$TS.log"

NMAP_CMD="nmap -n -sP"
if [ "$2" == "txt" ]
then
	NMAP_CMD="$NMAP_CMD -oX $LOG_FILE"
else
	NMAP_CMD="$NMAP_CMD -oN $LOG_FILE"
fi

if [[ "$1" =~ ".*/.*" ]]
then
	$NMAP_CMD -p "$1" | grep report | awk '{print $5}'
elif [ -f "$1" ]
then
	$NMAP_CMD -iL "$1" | grep report | awk '{print $5}'
else
	$NMAP_CMD $1 | grep report | awk '{print $5}'
fi

echo ""
echo "log file: $LOG_FILE"
echo ""
