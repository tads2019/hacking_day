#!/bin/bash

# contributors: kd, 

[ $1 ] && [ $2 ] && [ $3 ] && [ -f $3 ] || { echo "Usage: $0 <host|network|file.txt> <results_format: txt|xml> <script.LUA>"; exit; }

[ -d logs ] || { mkdir logs; }
TS=`date +%Y%m%d%H%M%S`
LOG_FILE="logs/nmap-$TS.log"

NMAP_CMD="nmap -Pn --script $3"
if [ "$2" == "txt" ]
then
	NMAP_CMD="$NMAP_CMD -oX $LOG_FILE"
else
	NMAP_CMD="$NMAP_CMD -oN $LOG_FILE"
fi

if [[ "$1" =~ ".*/.*" ]]
then
	$NMAP_CMD -p "$1"
elif [ -f "$1" ]
then
	$NMAP_CMD -iL "$1"
else
	$NMAP_CMD $1
fi
