#!/bin/bash

# contributors: kd, 

[ $1 ] && [ -f $1 ] || { echo "Usage: $0 <file_with_hosts.txt>"; exit; }

[ -d logs ] || { mkdir logs; }
TS=`date +%Y%m%d%H%M%S`
LOG_FILE="logs/nmap-$TS.log"

nmap -oN $LOG_FILE -iL "$1"

echo ""
echo "log file: $LOG_FILE"
echo ""
