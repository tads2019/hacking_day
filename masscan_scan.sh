#!/bin/bash

# contributors: kd, 

[ $1 ] && [ $2 ] || { echo "Usage: $0 <network (e.g. \"192.168.1.0/24\")> <ports: most_common|well_known|first_9000|all>"; exit; }

[ -d logs ] || { mkdir logs; }
TS=`date +%Y%m%d%H%M%S`
LOG_FILE="logs/masscan-$TS.log"

MASSCAN_CMD="sudo masscan --rate 100000000 --ttl 5 --wait 2 --output-filename $LOG_FILE --output-format grepable"
if [ "$2" == "most_common" ]
then
	PORTS="-p20,21,22,23,25,80,110,143,139,443,465,587,993,995,1433,1521,1630,3306,3938,5432"
elif [ "$2" == "well_known" ]
then
	PORTS="-p0,1-1024"
elif [ "$2" == "first_9000" ]
then
	PORTS="-p0,1-9000"
else
	PORTS="-p0,1-65535"
fi
MASSCAN_CMD="$MASSCAN_CMD $PORTS"

if [[ $1 == *"/"* ]]
then
	$MASSCAN_CMD "$1"
else
	echo "[WARNING] Invalid network \"$1\""
    exit
fi

echo ""
echo "log file: $LOG_FILE"
echo ""
