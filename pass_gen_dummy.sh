#!/bin/bash

SIZE=$((RANDOM%12+8))
cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-$SIZE} | head -n 1

