#!/bin/bash

# contributors: rb, kd, ..

if [ $# -lt 3 ]
then
	echo "Usage: $0 <target_ip> <num_of_requests> <num_of_cores>"
	exit 1
fi

pushd third_party/htstress

[ -f htstress ] || { bash build.sh; }

./htstress $1 -c $2 -t $3

popd
