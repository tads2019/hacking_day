#!/bin/bash

# contributors: kd, 

[ $1 ] && [ -f $1 ]  || { echo "Usage: $0 <lista_de_IPs.txt>"; exit; }

while read TARGET
do
    for PORT in `seq 1 65535`
    do 
        nc -v -z $TARGET $PORT
    done
done < $1

