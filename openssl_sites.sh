#!/bin/sh

# contributors: kd, 

[ $1 ] && [ -f $1 ] && [ $2 ] && [ -f $2 ] || { echo "Usage: $0 <openssl_scan.sh> <files_sites.txt>"; exit; }

STATSDIR=stats

[ -d $STATSDIR ] || { mkdir $STATSDIR; }

for SITE in `cat $2`
do
    echo -n "./checkSSL.sh $SITE 443 &> $STATSDIR/$SITE.stats & ... "
    ./$1 $SITE 443 &> $STATSDIR/$SITE.stats & 
    echo "done."
done
