#!/bin/bash

[ "$1" ] || { echo "Usage: $0 <option:install|execute>"; exit; }

if [ "$1" = "install" ]
then
    sudo apt-get update
    sudo apt-get install libnl-3-dev libnl-genl-3-dev pkg-config libssl-dev net-tools git sysfsutils python-scapy python-pycryptodome
    git clone https://github.com/vanhoefm/krackattacks-scripts
    cd krackattacks-scripts/hostapd
    hostapd
    make -j 2
    cd ../krackattack
    KPATH=$(pwd)
    echo "export PATH=\$PATH:\$KPATH" >> ~/.bashrc
else
    pushd krackattacks-scripts/krackattack
    krack-test-client.py --replay-broadcast
    krack-test-client.py --group --gtkinit
    krack-test-client.py --group
    krack-test-client.py
    popd
fi

