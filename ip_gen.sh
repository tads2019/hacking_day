#!/bin/bash

# contributors: kd, 

echo "A few examples... use one of your choice."

echo ""

sed 's/^//g' <<SHAR_EOF
seq -f "192.168.1.%g" 1 254

echo 192.168.{1..255}.{1..255} | tr ' ' '\012'

nmap -sL 192.168.64.0/27 | awk '{ print $5 }' | grep -v [a-z]

prips 192.168.1.0/24 
SHAR_EOF

echo ""
