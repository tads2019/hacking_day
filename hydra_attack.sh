#!/bin/bash

#contributors: bisso

#instalação da ferramenta:
#    
#    sudo apt install hydra
#
#    git clone https://github.com/vanhauser-thc/thc-hydra
#    cd thc-hydra/
#    ./configure
#    make
#    sudo make install

[ $1 ] && [ $2 ] && [ $3 ] || { echo"Uso: $0 <user> <worldlist> <url>";exit; }

hydra -t 4 -V -f -l $1 -P $2 $3

