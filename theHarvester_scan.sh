#!/bin/bash

# contributors: cr0d

[ $1 ] || [ $2 ] || { echo "Usage: $0 <domain (e.g unipampa.edu.br)> <search engine (e.g google)>"; exit; }

sudo pip3 install -r third_party/theHarvester/requirements.txt

sudo python3 third_party/theHarvester/theHarvester.py -d $1 -b $2


