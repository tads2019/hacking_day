#!/bin/bash

pkgs='libssl-dev'
if ! dpkg -s $pkgs >/dev/null 2>&1; then
  sudo apt-get install $pkgs
fi
pkgs='build-essential'
if ! dpkg -s $pkgs >/dev/null 2>&1; then
  sudo apt-get install $pkgs
fi

pushd third_party/rdpscan

[ -f rdpscan ] && echo "######## RDPSCAN ########" || make

if [ $# -lt 3 ]
then
    echo ""
	echo "Usage $0 <#_of_workers> <first_IP> <last_IP>"
    echo ""
	echo "Example: $0 1500 192.168.1.1 192.168.1.255"
    echo ""
	exit 1
fi

./rdpscan --workers $1 $2-$3

popd
